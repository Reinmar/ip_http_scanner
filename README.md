# IPSca

- Universal scanner for web interfaces of IoT devices
- Brute force function is embedded
- Graphic interface

![IPSCA](dict/example.jpg)

## Requirements
- Python 3 - install
- Python modules: `pip install -r requirements.txt`

## Disclaimer
- For educational use only