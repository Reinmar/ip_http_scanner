import sys
import time
import requests
from random import shuffle
import urllib3

from main_ui import *
from analyser import *
from logger import Logger
from brute import *

from threading import Thread
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import (QMainWindow, QApplication, QWidget,
                             QPushButton, QAction, QLineEdit, QMessageBox, QFileDialog)


class MyWin(QMainWindow):

    def __init__(self, parent=None):
        super(MyWin, self).__init__(parent)
        QWidget.__init__(self, parent)
        self.ui = Ui_IPSca()
        self.ui.setupUi(self)
        self.setWindowIcon(QIcon('ipsca.jpg'))

        self.logger = Logger()
        self.Scan = None
        self.IoTOnly = False
        self.brute_enable = False
        self.request_timeout_counter = 0

        self.ui.scan_btn.clicked.connect(self.click_scan)
        self.ui.import_btn.clicked.connect(self.file_open)
        self.ui.stop_btn.clicked.connect(self.click_stop)
        self.ui.shuffle_btn.clicked.connect(self.shuffle_hosts)
        self.ui.iot_chbx.stateChanged.connect(self.iot_only_change)
        self.ui.brute_chbx.stateChanged.connect(self.brute_enable_change)

    def iot_only_change(self):
        if self.IoTOnly:
            self.IoTOnly = False
        else:
            self.IoTOnly = True

    def brute_enable_change(self):
        if self.brute_enable:
            self.brute_enable = False
        else:
            self.brute_enable = True

    def file_open(self):
        name, _ = QFileDialog.getOpenFileName(self, 'Open File', options=QFileDialog.DontUseNativeDialog)
        if len(name) > 0:
            file = open(name, 'r')
        else:
            return False
        with file:
            text = file.read()
            self.ui.targetsList.setText(text)

    def shuffle_hosts(self):
        hosts = (self.ui.targetsList.toPlainText()).split('\n')
        shuffle(hosts)
        self.ui.targetsList.setText("\n".join(hosts))

    def click_scan(self):
        self.Scan = Scan()
        self.Scan.change_value.connect(self.update_terminal)
        self.Scan.change_progress_str.connect(self.update_progress)
        self.Scan.change_progressBar.connect(self.setProgressVal)

        self.Scan.start()

    def click_stop(self):
        try:
            if self.Scan.isScan:
                self.Scan.stop()
                self.logger.close_all()
                self.ui.terminal.append('<b>[-] Canceling...</b>')
        except AttributeError:
            pass

    def update_terminal(self, val):
        self.ui.terminal.append(val)

    def update_progress(self, val):
        self.ui.progress.setText(val)

    def setProgressVal(self, val):
        # print(self.Scan.change_progressBar)
        self.ui.progressBar.setValue(int(val))


class Scan(QThread):

    change_value = pyqtSignal(str)
    change_progress_str = pyqtSignal(str)
    change_progressBar = pyqtSignal(int)

    t = []

    def __init__(self, parent=None,):
        super(Scan, self).__init__(parent)

        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)'}
        self.HOSTS = (myapp.ui.targetsList.toPlainText()).split('\n')
        self.total = len(self.HOSTS)
        self.filter = str(myapp.ui.filter_line.text())
        self.isScan = True
        try:
            self.THREADS = int(myapp.ui.threads_edit.text())
            self.TIMEOUT = int(myapp.ui.timeout_edit.text())
        except:
            QMessageBox.about(myapp, 'Error', 'Input can only be a number')
            self.isScan = False

    def send_signal(self, sig):
        self.change_value.emit(sig)

    def stop(self):
        for thread in self.t:
            thread.join()
        self.t = []
        self.isScan = False

    def run(self):
        for i, host in enumerate(self.HOSTS):
            if not self.isScan: break
            self.change_progress_str.emit(f'<b>[{i+1}/{self.total}]{host} <span style=\"color:#FF0000;\">{myapp.request_timeout_counter}</span></b>')
            self.change_progressBar.emit(int((i*100)/len(self.HOSTS)))
            thread = Thread(target=self.discover, args=(host,))
            self.t.append(thread)
            thread.start()
            if (i + 1) % self.THREADS == 0 or (i + 1) == len(self.HOSTS):
                [_.join(2) for _ in self.t]
                self.t = []
        # self.change_progressBar.emit(100)
        self.stop()



    def discover(self, host):
        try:
            response = requests.get(f'http://{host}', timeout=self.TIMEOUT, headers=self.headers)
            if not self.filter in str(response.headers):
                return False
            analys = Analyser(str(response.headers) + cut(response.text, '<title>', '</title>'))
            if analys.done:
                result, device, vendor = analys.make_results()
                myapp.logger.logging(device, vendor, host)
                if myapp.brute_enable:
                    if vendor == 'Hikvision':
                        brute = Brute_Hik(response.url)
                    else:
                        brute = Brute_GET(response)
                    brute.start()
                    result += brute.make_results()
                self.send_signal(f"<b>[{host}] {result}</b>")
            elif 'WWW-Authenticate' in str(response.headers) and not myapp.IoTOnly:
                if myapp.brute_enable:
                    brute = Brute_GET(response)
                    brute.start()
                    result = brute.make_results()
                self.send_signal(f"<b>[{host} {(response.headers['WWW-Authenticate']).split(',')[0]} {result}</b>")
            elif 'Server' in str(response.headers) and not myapp.IoTOnly:
                self.send_signal(f"<b>[{host}]</b> {prepare_html(response)}")

        except requests.exceptions.ReadTimeout:
            myapp.request_timeout_counter += 1

        except urllib3.exceptions.ReadTimeoutError:
            myapp.request_timeout_counter += 1
        except Exception as e:
             #self.send_signal(str(e))
             pass

def prepare_html(response):
    server = response.headers['Server']
    title = cut(response.text, '<title>', '</title>') \
        if 'JavaScript' not in cut(response.text, '<title>', '</title>') else ''
    return (f'{server} {title}')[:45]

def cut(content, begin, end):
    idx1 = content.find(begin)
    idx2 = content.find(end, idx1)
    return content[idx1+len(begin):idx2].strip()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    myapp = MyWin()
    myapp.show()
    sys.exit(app.exec_())

