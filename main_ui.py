# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'scanner_gui.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_IPSca(object):
    def setupUi(self, IPSca):
        IPSca.setObjectName("IPSca")
        IPSca.resize(453, 691)
        self.scan_btn = QtWidgets.QPushButton(IPSca)
        self.scan_btn.setGeometry(QtCore.QRect(270, 290, 81, 23))
        self.scan_btn.setObjectName("scan_btn")
        self.import_btn = QtWidgets.QToolButton(IPSca)
        self.import_btn.setGeometry(QtCore.QRect(130, 20, 31, 21))
        self.import_btn.setObjectName("import_btn")
        self.targetsList = QtWidgets.QTextEdit(IPSca)
        self.targetsList.setGeometry(QtCore.QRect(10, 50, 151, 241))
        font = QtGui.QFont()
        font.setItalic(False)
        font.setUnderline(False)
        self.targetsList.setFont(font)
        self.targetsList.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.targetsList.setObjectName("targetsList")
        self.terminal = QtWidgets.QTextBrowser(IPSca)
        self.terminal.setGeometry(QtCore.QRect(20, 360, 421, 191))
        font = QtGui.QFont()
        font.setBold(False)
        font.setWeight(50)
        self.terminal.setFont(font)
        self.terminal.setObjectName("terminal")
        self.progress = QtWidgets.QTextBrowser(IPSca)
        self.progress.setGeometry(QtCore.QRect(20, 590, 421, 31))
        self.progress.setObjectName("progress")
        self.label = QtWidgets.QLabel(IPSca)
        self.label.setGeometry(QtCore.QRect(10, 20, 121, 21))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.threads_edit = QtWidgets.QLineEdit(IPSca)
        self.threads_edit.setGeometry(QtCore.QRect(400, 50, 41, 20))
        self.threads_edit.setObjectName("threads_edit")
        self.label_2 = QtWidgets.QLabel(IPSca)
        self.label_2.setGeometry(QtCore.QRect(350, 50, 71, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_2.setFont(font)
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(IPSca)
        self.label_3.setGeometry(QtCore.QRect(310, 80, 111, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.timeout_edit = QtWidgets.QLineEdit(IPSca)
        self.timeout_edit.setGeometry(QtCore.QRect(400, 80, 41, 20))
        self.timeout_edit.setObjectName("timeout_edit")
        self.progressBar = QtWidgets.QProgressBar(IPSca)
        self.progressBar.setGeometry(QtCore.QRect(20, 560, 421, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.progressBar.setFont(font)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")
        self.stop_btn = QtWidgets.QPushButton(IPSca)
        self.stop_btn.setGeometry(QtCore.QRect(360, 290, 81, 23))
        self.stop_btn.setObjectName("stop_btn")
        self.filter_line = QtWidgets.QLineEdit(IPSca)
        self.filter_line.setGeometry(QtCore.QRect(130, 329, 311, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.filter_line.setFont(font)
        self.filter_line.setObjectName("filter_line")
        self.label_5 = QtWidgets.QLabel(IPSca)
        self.label_5.setGeometry(QtCore.QRect(20, 330, 101, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.shuffle_btn = QtWidgets.QPushButton(IPSca)
        self.shuffle_btn.setGeometry(QtCore.QRect(10, 290, 151, 23))
        self.shuffle_btn.setObjectName("shuffle_btn")
        self.iot_chbx = QtWidgets.QCheckBox(IPSca)
        self.iot_chbx.setGeometry(QtCore.QRect(360, 120, 81, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.iot_chbx.setFont(font)
        self.iot_chbx.setObjectName("iot_chbx")
        self.brute_chbx = QtWidgets.QCheckBox(IPSca)
        self.brute_chbx.setGeometry(QtCore.QRect(360, 150, 97, 25))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.brute_chbx.setFont(font)
        self.brute_chbx.setObjectName("brute_chbx")

        self.retranslateUi(IPSca)
        QtCore.QMetaObject.connectSlotsByName(IPSca)

    def retranslateUi(self, IPSca):
        _translate = QtCore.QCoreApplication.translate
        IPSca.setWindowTitle(_translate("IPSca", "IPSca"))
        self.scan_btn.setText(_translate("IPSca", "Scan"))
        self.import_btn.setText(_translate("IPSca", "..."))
        self.targetsList.setHtml(_translate("IPSca", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Roboto\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'MS Shell Dlg 2\'; font-size:10pt;\">127.0.0.1:8080</span></p></body></html>"))
        self.progress.setHtml(_translate("IPSca", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'Roboto\'; font-size:11pt; font-weight:400; font-style:normal;\">\n"
"<p align=\"center\" style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:\'MS Shell Dlg 2\'; font-size:8.25pt;\"><br /></p></body></html>"))
        self.label.setText(_translate("IPSca", "Targets (ip:port)"))
        self.threads_edit.setText(_translate("IPSca", "5"))
        self.label_2.setText(_translate("IPSca", "Threads"))
        self.label_3.setText(_translate("IPSca", "SocketTimeout"))
        self.timeout_edit.setText(_translate("IPSca", "1"))
        self.stop_btn.setText(_translate("IPSca", "Stop"))
        self.label_5.setText(_translate("IPSca", "Filter (optional):"))
        self.shuffle_btn.setText(_translate("IPSca", "Shuffle"))
        self.iot_chbx.setText(_translate("IPSca", "IoT only"))
        self.brute_chbx.setText(_translate("IPSca", "Bruteforce"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    IPSca = QtWidgets.QDialog()
    ui = Ui_IPSca()
    ui.setupUi(IPSca)
    IPSca.show()
    sys.exit(app.exec_())

