from logger import Logger

# logger = Logger()

class Analyser:

    def __init__(self, data):

        self.data = str(data)
        self.device = ''
        self.vendor = ''
        self.vulnerabilities = ['uc-httpd 1.0.0', 'thttpd/2.25b', 'P3P: CP=CAO PSA OUR', 'GoAhead+5ccc069c403ebaf9f0171e9517f40e41&']
        self.cameras = {'Hipcam': ['realm="index.html"'],
                        'Hikvision': ['App-webs/', 'DNVRS-Webs', 'DVRDVS-Webs', 'Hikvision-Webs', 'webserver'],
                        'GoAhead': ['GoAhead-Webs', 'realm="WIFICAM"', 'realm="Internet Camera"', 'GoAhead-http'],
                        'ActiveCam': ['ActiveCam'],
                        'NETSurveillance': ['uc-httpd 1.0.0'],
                        'BEWARD': ['N100 H.264 IP Camera'],
                        'D-Link': ['DCS-', 'DI-'],
                        'AXIS': ['AXIS'],
                        'Foscam': ['IPCam Client'],
                        '???': ['WebCam', 'Camera', 'DVR', 'NVR']}

        self.routers = {'TP-LINK': ['TP-LINK', 'TL-WR'],
                        'ZyXEL': ['ZyXEL'],
                        'D-LINK': ['D-LINK'],
                        'ASUS': ['RT-N', 'RT-G', 'RT-AC', 'RT-AX'],
                        'CISCO': ['level_15 or view_access', 'level_15_access'],
                        'DD-WRT': ['DD-WRT'],
                        'MikroTik': ['Mikrotik']}
        self.done = self.identifying_device()
        self.vulnerability = self.isVulnerability()


    def identifying_device(self):
        for cam in self.cameras:
            if any([c in self.data for c in self.cameras[cam]]):
                self.vendor = cam
                self.device = 'IPCAM'
                return True
        for rou in self.routers:
            if any([r in self.data for r in self.routers[rou]]):
                self.vendor = rou
                self.device = 'ROUTER'
                return True
        return False

    def isVulnerability(self):
        for vul in self.vulnerabilities:
            if vul in self.data:
                return True
        return False

    def make_results(self):
        res = f'<b>[<span style=\"color:#4cbb17;\">{self.vendor}</span>][<span style=\"color:#0000FF;\">{self.device}</span>]'
        if self.vulnerability:
            res += '[<span style=\"color:#FF0000;\">VULN</span>]</b>'
        else:
            res += '</b>'
        return res, self.device, self.vendor
